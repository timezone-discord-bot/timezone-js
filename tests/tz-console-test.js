require('../tz-console');

console.error('111 log test string', {'someStr':'also','someInt':34, 'someObj': {'innerTxt':'value24', 'innerDte':new Date()}});
console.log('111 log test string', {'someStr':'also','someInt':34, 'someObj': {'innerTxt':'value24', 'innerDte':new Date()}});

console.error('222 log frmt %o str ', {'someObjProp': 456.123});
console.log('222 log frmt %o str ', {'someObjProp': 456.123});

console.error('333 log frmt %o str ', '[some other txt]');
console.log('333 log frmt %o str ', '[some other txt]');

console.error('444 yo ho', 'barrels of rum');
console.log('444 yo ho', 'barrels of rum');

console.error({'testError':'val1'});
console.log({'testLog':'val2'});
console.warn({'testWarn':'val3'});

try {
    throw new Error('test error');
}
catch(err) {
    console.error(err);
    console.log(err);
    console.warn(err);
}