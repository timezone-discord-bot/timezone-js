const tzTime = require('..\\tz-time');

var badComp = tzTime.getTimeComponents('invalid time');
console.log(badComp);

var timeComp = tzTime.getTimeComponents('12:19am');
console.log(timeComp);

var gmt_offset = tzTime.getGMTOffset(timeComp.hours, timeComp.tt);
console.log(gmt_offset);

console.log(tzTime.getCurrentTimeOffset(gmt_offset));

