'use strict';
// console override 
require('./tz-console');
// third party imports
const Discord = require('discord.js');
// our imports 
const TzSettings = require('./tz-settings').TzSettings;
const TzLoop = require('./tz-bot-loop');
const TzCommands = require('./tz-commands');
const TzReactions = require('./tz-reactions');

class TzListen {
    constructor(options, settings) {
        this._discordClient = new Discord.Client();
        this._tzSettings = {};
        this._tzCommands = {};
        this._tzLoop = new TzLoop();
        if (settings) {
            this._tzSettings = settings; 
        }
        else {
            this._tzSettings = new TzSettings();
        }
        this._tzCommands = new TzCommands(this._tzSettings, this._discordClient);
        this._tzReactions = new TzReactions(this._tzSettings, this._discordClient);
        this._tzLoop = new TzLoop();
    }
    
    init() {

        // once we have connection to discord via websockets 
        // we will get this ready event firing off so we use 
        // this to trigger the bot's main (console) command loop 
        this._discordClient.on('ready', () => {
            console.log(`Logged in as ${this._discordClient.user.tag}!`);
            this._tzLoop.commandLoop("TZ-LISTEN> ");
        });

        // Bot added to Discord Server
        this._discordClient.on('guildCreate', guild => {
            console.log(`Guild Added - Id: ${guild.id}, Name: ${guild.name}`);
            console.log(guild);
            if (guild.systemChannel) {
                this._tzCommands.sendInitialGreeting(guild.systemChannel);
            }
            else if (guild.systemChannelID && 
                     guild.channels && 
                     guild.channels.has(guild.systemChannelID)) {
                this._tzCommands.sendInitialGreeting(guild.channels.get(guild.systemChannelID));
            }
        });

        // Bot removed from Discord Server
        this._discordClient.on('guildDelete', guild => {
            console.log(`Guild Removed - Id: ${guild.id}, Name: ${guild.name}`);
        });

        // the event that tells us a message has been sent by someone
        // or something on a discord server this bot has been added to. 
        // we need to do some basic filtering here because this fires 
        // for literally every fricken god damn stupid message. 
        // specifically, we make sure it is not a system message, that 
        // the message author is not another bot, and then we make sure 
        // that the message contents either begin with our special !tz 
        // prefix or that this bot has been @mentioned by nathis._ 
        this._discordClient.on('message', msg => {
            // don't pay attention to system or bot messages at all 
            if (msg.system || 
                msg.author.bot) {
                return;
            }
            // make sure this bot has been asked to do something... 
            if (msg.content.startsWith('!tz') || // !tz command prefix used 
                msg.isMemberMentioned(this._discordClient.user) || // @mention used 
                msg.channel.type == 'dm') { // direct message 
                this._tzCommands.handleDiscordCommand(msg); 
            }
            // otherwise parse for reaction if enabled
            else {
                this._tzReactions.parseForReaction(msg);
            }
        });

        this._discordClient.on('messageReactionAdd', (msgReact, user) => {
            console.log('messageReactionAdd=>msgReact', msgReact);
            if (msgReact.me) { 
                this._tzReactions.reactToTimeText(user, msgReact); 
            }
        });

        // not sure if we will ever see this fire... 
        this._discordClient.on('rateLimit', rateLimitInfo => {
            console.error(`Discord Rate Limit!`, rateLimitInfo);
        });

        // need to implement error handling later... 
        this._discordClient.on('error', errorInfo => {
            console.error(`Discord Error!`, errorInfo);
        });

        // aaannnnddddd.... we login to discord to begin our journey! 
        this._discordClient.login(this._tzSettings.discord.token);
    }
}

var tzListen = new TzListen();
tzListen.init();






