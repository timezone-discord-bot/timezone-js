'use strict';
const readline = require('readline');
const normalizeWhitespace = require('normalize-html-whitespace');
const stringArgv = require('string-argv');
const jslinq = require('jslinq');
const tzString = require('./tz-string'); 

class TzLoop {
    constructor(options) {
        this._altCmds = {};
        this._exitMessage = 'Goodbye!';
        this._commandCallback = function () {};
        // alt cmd maps
        if (options && options.altCommands && typeof options.altCommands == 'Map' && options.altCommands.size > 0) {
            this._altCmds = options.altCommands; 
        }
        else {
            this._altCmds = new Map(); 
        }
        // exit alt cmds
        if (options && Array.isArray(options.exit) && options.exit.size > 0) {
            var exitCmds = new jslinq(options.exit).where(e => tzString.hasText(e)).select(e => e.trim().toLowerCase()).items; 
            for(var i = 0; i < exitCmds.size; i++) {
                this._altCmds.set(exitCmds[i], 'exit'); 
            }
        }
        else if (options && tzString.hasText(options.exit))
        {
            this._altCmds.set(options.exit.trim().toLowerCase(), 'exit'); 
        }
        // exit message 
        if (options && tzString.hasText(options.exitMessage)) {
            this._exitMessage = options.exitMessage; 
        }
        // commandCallback 
        if (options && options.commandCallback && typeof options.commandCallback == 'function') {
            this._commandCallback = function(cmd) {
                options.commandCallback(cmd); 
                // possibly add some global command logging here later... 
                // tzLog.logConsoleCommand(cmd); 
            }
        }
        else { 
            this._commandCallback = function(cmd) {
                // possibly add some global command logging here later... 
                // tzLog.logConsoleCommand(cmd); 
            };
        }
    }
    
    get exit() {
        return this._exit;
    }
    get exitMessage() {
        return this._exitMessage;
    }
    get commandCallback() {
        return this._commandCallback; 
    }
    async consoleInput(query) { 
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        return new Promise(resolve => rl.question(query, ans => {
            rl.close();
            resolve(ans);
        }));
    }

    async handleConsoleCommand(cmd) { 
        var cmdArgs = null;
        if (!cmd || cmd == null || cmd.trim() == '') { 
            cmdArgs = [];
        }
        else {
            cmdArgs = stringArgv(normalizeWhitespace(cmd.toLowerCase().trim()));
        }
        if (cmdArgs.length > 0) {
            var command = cmdArgs[0]; 
            if (this._altCmds.has(command))
                command = _altCmds.get(command);
            var args = cmdArgs.size > 1 ? new jslinq(cmdArgs).skip(1).items : [];
            switch (command) {
                case 'exit': 
                {
                    console.log(this.exitMessage);
                    setTimeout(() => { process.exit(0); }, 500);
                    return false;
                }
                /* case 'status': 
                {
                    
                    break;
                } */
            }
        }
        return true;
    }
    async commandLoop(prompt) { 
        var cmd = (await this.consoleInput(prompt)).toLowerCase().trim();  
        if (await this.handleConsoleCommand(cmd)) {
            if (this.commandCallback)
                this.commandCallback(cmd);
            await this.commandLoop(prompt);
        }
        return new Promise(resolve => resolve(true));
    }
}

module.exports = TzLoop; 