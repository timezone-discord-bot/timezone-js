'use strict';
const normalizeWhitespace = require('normalize-html-whitespace');
const stringArgv = require('string-argv');
const jslinq = require('jslinq');
const tzString = require('./tz-string');
const TzGuildSettings = require('./tz-guild-settings');
const TzUserSettings = require('./tz-user-settings');
const tzData = require('./tz-data'); 
const tzTime = require('./tz-time');

class TzCommands {
    constructor(settings, discordClient) { 
        this._settings = settings;
        this._discordClient = discordClient;
        this._guildSettings = new TzGuildSettings(this._settings); 
        this._userSettings = new TzUserSettings(this._settings);
    }
    get options() {
        return this._settings; 
    }
    get discordClient() {
        return this._discordClient; 
    }
    get currentUser() {
        if ((this._discordClient) && (this._discordClient.user)) {
            return this._discordClient.user;
        }
        return null;
    }

    async handleDiscordCommand(msg) {
        //console.log(`\r\nCommand Msg Received - GuildId: ${msg.guild.id} ChannelId: ${msg.channel.id} UserId: ${msg.author.id} Content: ${msg.content}`); 

        var tmpArgs = null;
        if (msg.content.startsWith('!tz')) {
            tmpArgs = normalizeWhitespace(msg.content.substring(4).trim());
        } 
        else { 
            tmpArgs = normalizeWhitespace(msg.content.replace(`<@${this._discordClient.user.id}>`, '').trim());
        }
        var args = null;
        if (tmpArgs == '') { 
            args = [];
        }
        else {
            args = stringArgv(tmpArgs); 
            //console.log(`Arguments: ${args.join(', ')}`);
        }
        var response = await this.executeCommand(args, msg); 
        if (Array.isArray(response)) {
            for(var i = 0; i < response.length; i++) {
                await msg.reply(response[i]); 
            }
        }
        else {
            await msg.reply(response);
        }

        return new Promise(resolve => { resolve(true); });
    }

    getNoServerCmdInDmResponse() {
        return `Sorry- I cannot assist with commands related to a Discord server when using direct messages (DM). \n` + 
                `These are the commands I can help you with: \n` + 
                `\t- _help_\n` + 
                `\t- _mystatus/mysettings/config_\n` + 
                `\t- _time_\n` + 
                `\t- _setmydst_\n`;
    }

    async executeCommand(args, msg) {
        var response = '';
        
        if (!args || args.length == 0) {
            response = 'I\'m sorry I didn\'t understand you. Try using **help**.'; 
        }
        else { 
            //console.log(msg);
            var isAdmin = msg.member && msg.member.hasPermission('ADMINISTRATOR'); 
            var isDm = msg.channel.type == 'dm';
            var cmd = args[0].toLowerCase(); 
            args = new jslinq(args).skip(1);
            switch (cmd)
            {
                case 'reaction':
                {
                    if (isDm) {
                        response = this.getNoServerCmdInDmResponse(); 
                    }
                    else {
                        if (isAdmin) {
                            var currentStatus = await this._guildSettings.toggleGuildReaction(msg.guild.id, msg.author.id); 
                            response = `My ***reactions*** feature is now turned **${currentStatus ? 'on' : 'off'}**.`;
                        }
                        else {
                            var currentStatus = await this._guildSettings.isGuildReactionOn(msg.guild.id); 
                            response = `My ***reactions*** feature is currently **${currentStatus ? 'on' : 'off'}**.\r\n\r\n` + 
                                       `You do NOT have the Admin privilege to the '${msg.guild.name}' Discord server and cannot change this setting.`;
                        }
                    }
                    break;
                }
                case 'serverstatus':
                case 'serversettings':
                case 'serverconfig':
                {
                    if (isDm) {
                        response = this.getNoServerCmdInDmResponse(); 
                    }
                    else {
                        if (isAdmin) {
                            var guildSettings = null;
                            await this._guildSettings.getGuildSettings(msg.guild.id)
                                .then(function(result) { guildSettings = result; })
                                .catch(function(err) { console.error(err); });
                            if (guildSettings) { 
                                response = `Here are the settings I have stored for this Discord Server: \r\n` + 
                                        `\t- Reactions Feature: ${guildSettings.reaction_on === 1 ? 'ON' : 'OFF' } \r\n` + 
                                        `\t- Current Server Time: ${guildSettings.gmt_offset ? tzTime.getCurrentTimeOffset(guildSettings.gmt_offset) : 'Not Set'} \r\n` + 
                                        `\t- Daylight Saving Time: ${guildSettings.dst ? guildSettings.dst === 1 ? 'ON' : 'OFF' : 'Not Set (default is OFF)' } \r\n`; 
                            }
                            else {
                                response = `I do not have any settings stored for this Discord Server.`; 
                            }
                        }
                        else { 
                            response = `You do NOT have the Admin privilege to the '${msg.guild.name}' Discord server and cannot use this command.`;
                        }
                    }
                    break;
                }
                case 'serverreset':
                {
                    if (isAdmin) {
                        var me = this; 
                        await tzData.getConnection(me._settings.mysql).then(async function(dbConn) {
                            await me._guildSettings.getGuildSettings(msg.guild.id, dbConn).then(async function(guildSettings) { 
                                if (guildSettings) { 
                                    guildSettings.reaction_on = null;
                                    guildSettings.gmt_offset = null;
                                    guildSettings.dst = null;
                                    await me._guildSettings.saveGuildSettings(guildSettings, msg.author.id, dbConn).then(async function(saveResults) { 
                                        response = `I reset the settings for this Discord server. \r\n`;
                                        await me._guildSettings.getGuildSettings(msg.guild.id, dbConn).then(async function(guildSettings) { 
                                            response += `\t- Reactions Feature: ${guildSettings.reaction_on === 1 ? 'ON' : 'OFF' } \r\n` + 
                                                        `\t- Current Server Time: ${guildSettings.gmt_offset ? tzTime.getCurrentTimeOffset(guildSettings.gmt_offset) : 'Not Set'} \r\n` + 
                                                        `\t- Daylight Saving Time: ${guildSettings.dst ? guildSettings.dst === 1 ? 'ON' : 'OFF' : 'Not Set (default is OFF)' } \r\n`;  
                                        })
                                        .catch(function(err) {
                                            console.error(err);
                                            response = 'Oh dear! I seem to have experienced some kind of issue. Details have been logged.';            
                                        })
                                    })
                                    .catch(function(err) {
                                        console.error(err);
                                        response = 'Oh dear! I seem to have experienced some kind of issue. Details have been logged.';            
                                    })
                                }
                                else {
                                    response = `I do not have any settings stored for this Discord Server so there is nothing to reset.`; 
                                }
                            })
                            .catch(function(err) { 
                                console.error(err); 
                                response = 'Oh dear! I seem to have experienced some kind of issue. Details have been logged.';
                            })
                            .then(async function() {
                                await dbConn.endAsync();
                            })
                        })
                        .catch(function(err) {
                            console.error(err);
                            response = 'Oh dear! I seem to have experienced some kind of issue. Details have been logged.';
                        });
                    }
                    else { 
                        response = `You do NOT have the Admin privilege to the '${msg.guild.name}' Discord server and cannot use this command.`;
                    }
                    break;
                }
                case 'mystatus':
                case 'mysettings':
                case 'myconfig':
                case 'status':
                case 'settings':
                case 'config':
                {
                    var userSettings = await this._userSettings.getUserSettings(msg.author.id);
                    if (userSettings) {
                        response = `Here are the settings I have stored for you.\r\n` + 
                                   `\t- Your Time: ${userSettings.gmt_offset ? tzTime.getCurrentTimeOffset(userSettings.gmt_offset) : 'Not Set'} \r\n` + 
                                   `\t- Daylight Saving Time: ${userSettings.dst ? userSettings.dst === 1 ? 'ON' : 'OFF' : 'Not Set (default is OFF)' } \r\n`; 
                    }
                    else {
                        response = `I do not have any settings stored for you.`
                    }
                    break;
                }
                case 'setservertime':
                case 'servertime':
                {
                    if (isDm) {
                        response = this.getNoServerCmdInDmResponse(); 
                    }
                    else {
                        if (isAdmin) {
                            if (args && args.count() > 0) {
                                var time = args.items.join(' ');
                                var timeComp = tzTime.getTimeComponents(time);
                                if (timeComp) {
                                    var gmt_offset = tzTime.getGMTOffset(timeComp.hours, timeComp.tt);
                                    var guildSettings = {
                                        guild_id: msg.guild.id,
                                        gmt_offset: gmt_offset
                                    };
                                    var dbConn = await tzData.getConnection(this._settings.mysql);
                                    try {
                                        await this._guildSettings.saveGuildSettings(guildSettings, msg.author.id, dbConn);
                                        guildSettings = await this._guildSettings.getGuildSettings(guildSettings.guild_id, dbConn);
                                    }
                                    catch(err) {
                                        console.error(err);
                                    } 
                                    finally {
                                        if (dbConn) {
                                            await dbConn.endAsync(); 
                                        }
                                    }
                                    response = `Thanks! The default time for this Discord server has been set to ${tzTime.getCurrentTimeOffset(guildSettings.gmt_offset)}.`;
                                    if (!guildSettings.dst) {
                                        response += `\r\n\r\n_Reminder: I still don't know whether to use Daylight Saving Time for your Discord server, please let me know with the **serverdst** command. See **help** for more info._`;
                                    }
                                }
                                else {
                                    response = `I did not understand what you meant for the default time to use for the Discord server, see **help**.`;
                                }
                            }
                            else {
                                response = `You need to tell me what default time to use for the Discord server, see **help**.`;
                            }
                        }
                        else {
                            var guildSettings = await this._guildSettings.getGuildSettings(msg.guild.id);
                            if (guildSettings && 
                                guildSettings.gmt_offset) {
                                response = `Discord server default time is ${tzTime.getCurrentTimeOffset(guildSettings.gmt_offset)}.\r\n\r\n`;
                            }
                            else {
                                response = `Discord server does NOT have the default time set.\r\n\r\n`;
                            }
                            response += `You do NOT have the Admin privilege to the '${msg.guild.name}' Discord server and cannot change this setting.`;
                        }
                    }
                    break;
                }
                case 'setmytime':
                case 'mytime':
                case 'settime':
                case 'time':
                {
                    if (args && args.count() > 0) {
                        var time = args.items.join(' ');
                        var timeComp = tzTime.getTimeComponents(time);
                        if (timeComp) {
                            var gmt_offset = tzTime.getGMTOffset(timeComp.hours, timeComp.tt);
                            var userSettings = {
                                user_id: msg.author.id,
                                gmt_offset: gmt_offset
                            };
                            var dbConn = await tzData.getConnection(this._settings.mysql);
                            try {
                                await this._userSettings.saveUserSettings(userSettings, msg.author.id, dbConn);
                                userSettings = await this._userSettings.getUserSettings(userSettings.user_id, dbConn);
                            }
                            catch(err) {
                                console.error(err);
                            } 
                            finally {
                                if (dbConn) {
                                    await dbConn.endAsync(); 
                                }
                            }
                            response = `Thanks! Your time has been set to ${tzTime.getCurrentTimeOffset(userSettings.gmt_offset)}.`;
                            if (!userSettings.dst) {
                                response += `\r\n\r\n_Reminder: I still don't know whether to use Daylight Saving Time for your time, please let me know with the **dst** command. See **help** for more info._`;
                            }
                        }
                        else {
                            response = `I did not understand what you meant for your time, see **help**.`;
                        }
                    }
                    else {
                        response = `You need to tell me what your time is, see **help**.`;
                    }
                    break;
                }
                case 'setserverdst': 
                case 'serverdst': 
                {
                    if (isDm) {
                        response = this.getNoServerCmdInDmResponse(); 
                    }
                    else {
                        if (isAdmin) {
                            var dst = args.items.join(' ').trim().toLowerCase();
                            var onVals = [ 'yes', 'true', '1' ];
                            var offVals = [ 'no', 'false', '0' ];
                            if (dst && dst != '') {
                                if (onVals.find(function(el) { return el === dst; }) != undefined) {
                                    dst = 1
                                }
                                else if (offVals.find(function(el) { return el === dst; }) != undefined) {
                                    dst = 0;
                                }
                                else {
                                    dst = null;
                                    response = `I did not understand what you meant for whether to use Daylight Saving Time for the Discord server, see **help**.`;
                                }
                                if (dst) {
                                    var guildSettings = {
                                        guild_id: msg.guild.id,
                                        dst: dst
                                    };
                                    var dbConn = await tzData.getConnection(this._settings.mysql);
                                    try {
                                        await this._guildSettings.saveGuildSettings(guildSettings, msg.author.id, dbConn);
                                        guildSettings = await this._guildSettings.getGuildSettings(guildSettings.guild_id, dbConn);
                                    }
                                    catch(err) {
                                        console.error(err);
                                    } 
                                    finally {
                                        if (dbConn) {
                                            await dbConn.endAsync();
                                        }
                                    }
                                    response = `Thanks! The Daylight Saving Time usage for this Discord server has been set to ***${dst == 1 ? 'on' : 'off'}***.`;
                                    if (!guildSettings.gmt_offset) {
                                        response += `\r\n\r\n_Reminder: I still don't know what default time to use for your Discord server, please let me know with the **servertime** command. See **help** for more info._`;
                                    }
                                }
                            }
                            else {
                                response = `You need to tell me whether to use Daylight Saving Time for the Discord server, see **help**.`;
                            }
                        }
                        else {
                            var guildSettings = await this._guildSettings.getGuildSettings(msg.guild.id);
                            if (guildSettings && 
                                guildSettings.dst) {
                                response = `Discord server Daylight Saving Time usage is ***${guildSettings.dst == 1 ? 'on' : 'off'}***.\r\n\r\n`;
                            }
                            else {
                                response = `Discord server does NOT have Daylight Saving Time usage set.\r\n\r\n`;
                            }
                            response += `You do NOT have the Admin privilege to the '${msg.guild.name}' Discord server and cannot change this setting.`;
                        }
                    }
                    break;
                }
                case 'setmydst':
                case 'mydst':
                case 'setdst':
                case 'dst':
                {
                    var dst = args.items.join(' ').trim().toLowerCase();
                    var onVals = [ 'yes', 'true', '1' ];
                    var offVals = [ 'no', 'false', '0' ];
                    if (dst && dst != '') {
                        if (onVals.find(function(el) { return el === dst; }) != undefined) {
                            dst = 1
                        }
                        else if (offVals.find(function(el) { return el === dst; }) != undefined) {
                            dst = 0;
                        }
                        else {
                            dst = null;
                            response = `I did not understand what you meant for whether to use Daylight Saving Time for your time, see **help**.`;
                        }
                        if (dst) {
                            var userSettings = {
                                user_id: msg.author.id,
                                dst: dst
                            };
                            var dbConn = await tzData.getConnection(this._settings.mysql);
                            try {
                                await this._userSettings.saveUserSettings(userSettings, msg.author.id, dbConn);
                                userSettings = await this._userSettings.getUserSettings(userSettings.user_id, dbConn);
                            }
                            catch(err) {
                                console.error(err);
                            } 
                            finally {
                                if (dbConn) {
                                    await dbConn.endAsync();
                                }
                            }
                            response = `Thanks! The Daylight Saving Time usage for your time has been set to ***${dst == 1 ? 'on' : 'off'}***.`;
                            if (!userSettings.gmt_offset) {
                                response += `\r\n\r\n_Reminder: I still don't know what your time is, please let me know with the **time** command. See **help** for more info._`;
                            }
                        }
                    }
                    else {
                        response = `You need to tell me whether to use Daylight Saving Time for your time, see **help**.`;
                    }
                    break;
                }
                case 'help':
                {
                    if (isDm) {
                        response = `Here are the things I can help with. \r\n\r\n` +                     
                               `**settings**\tI\'ll let you know what settings I have stored for your user on this Discord Server: \r\n` + 
                               `          \t\t\t- your current _time_ \r\n` + 
                               `          \t\t\t- _daylight saving time_ on / off \r\n` + 
                               `          \t\tAlt commands: **status**, **config**, **mysettings**, **mystatus**, **myconfig**\r\n\r\n` + 
                               `**time**\tLet me know what time it is for you. This time will be used to provide you with time conversions.\r\n` + 
                               `          \t\tAlt commands: **mytime**, **setmytime**, **settime**\r\n\r\n` + 
                               `**dst**\tDoes your state or province observe Daylight Saving Time? ( yes/no | true/false | 1/0 )\r\n` + 
                               `          \t\tAlt commands: **mydst**, **setmydst**, **setdst**\r\n\r\n` + 
                               `For example, you could say: dst yes\r\n` + 
                               'Or, if you prefer: mydst 1\r\n\r\n' + 
                               `_NOTE: Commands available in Direct Messages (DM) are limited to the above. \r\n` + 
                               `For a full list of commands, ask me for ***help*** in one of your Discord server's channels._`;
                    }
                    else {
                        response = [];
                        response[0] = `Here are the things I can help with. \r\n` +                     
                               `You can ask for any of the below by using my **!tz** request prefix or if you prefer **@mention** me.\r\n\r\n` + 
                               `[USER COMMANDS]\r\n` + 
                               `**time**           \tLet me know what time it is for you. This time will be used to provide you with time conversions.\r\n` + 
                               `                   \t\tAlt commands: **mytime**, **setmytime**, **settime**\r\n\r\n` + 
                               `**dst**            \tDoes your state or province observe Daylight Saving Time? ( yes/no | true/false | 1/0 )\r\n` + 
                               `                   \t\tAlt commands: **mydst**, **setmydst**, **setdst**\r\n\r\n` + 
                               `**settings**       \tI\'ll let you know what settings I have stored for your user on this Discord Server: \r\n` + 
                               `                   \t\t\t- your current _time_ \r\n` + 
                               `                   \t\t\t- _daylight saving time_ on / off \r\n` + 
                               `                   \t\tAlt commands: **status**, **config**, **mysettings**, **mystatus**, **myconfig**\r\n\r\n`;
                        response[1] = `_help continued..._\r\n\r\n` + 
                               `[ADMIN/SERVER COMMANDS] _(require Admin permission on your Discord server)_\r\n` + 
                               `**reaction**       \tI\'ll turn my _reactions_ feature on or off.\r\n\r\n` + 
                               `**servertime**     \tIf you'd like to provide a default time for your Discord server, tell me with this command. \r\n` + 
                               `                   \tThis time will be used as a default when a user has not given me their time.\r\n` + 
                               `                   \t\tAlt command: setservertime\r\n\r\n` + 
                               `**serverdst**      \tUse this when providing a default time for your Discord server. This tells me whether to use \r\n` + 
                               `                   \tDaylight Saving Time with your server's default time. ( yes/no | true/false | 1/0 )\r\n` + 
                               `                   \t\tAlt command: setserverdst\r\n\r\n` + 
                               `**serversettings** \tI\'ll let you know what settings I have stored for your Discord Server: \r\n` + 
                               `                   \t\t\t- _reactions_ on / off\r\n` + 
                               `                   \t\t\t- current _discord server time_ \r\n` + 
                               `                   \t\t\t- _daylight saving time_ on / off \r\n` + 
                               `                   \t\tAlt commands: **serverstatus**, **serverconfig**\r\n\r\n` + 
                               `**serverreset**    \tI will reset all of the settings I have stored for this Discord server.\r\n` + 
                               `                   \t\t\t- _reactions_ will be turned OFF\r\n` + 
                               `                   \t\t\t- _discord server time_ will be deleted\r\n` + 
                               `                   \t\t\t- _daylight saving time_ will be deleted\r\n` + 
                               `                   \t\t_NOTE: This will effectively remove Discord server 'default time' if it has been set previously._\r\n\r\n` + 
                               `For example, you could say: <@${this._discordClient.user.id}> reaction\r\n` + 
                               'Or, if you prefer: !tz reaction';
                    }
                    break;
                }
                default: 
                {
                    response = 'I\'m sorry but I\'m not picking up what you\'re putting down.. try **help**.'; 
                    break;
                }
            }
        }

        return new Promise(resolve => { resolve(response); });
    }

    async sendInitialGreeting(textChannel) { 
        await textChannel.send(`Greetings! Thanks for adding me to your server. \r\n` + 
                               `For now I'll only speak up when @mentioned by name or if you use my !tz commands. _(***!tz help*** for more info)_ \r\n` + 
                               `If you'd like to enable my ***reactions*** feature use **!tz reaction** _(using it again turns it off)_.`); 
    }
}

module.exports = TzCommands; 