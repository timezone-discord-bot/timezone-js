const fs = require('fs');
const util = require('util');
const dateFormat = require('dateformat');

(function(){
    var _log = console.log;
    var _error = console.error;
    var _warning = console.warning;
    
    console.error = function () { 
        var logFile = fs.createWriteStream('tz-errors.log', { flags: 'a' });
        // Or 'w' to truncate the file every time the process starts.
        logFile.write(`${dateFormat(new Date(),'yyyy-mm-dd hh:MM:ss tt')} | ${util.format.apply(null, arguments)}\n\n`);
        logFile.close();
        _error.apply(console,arguments);
    }
  
    console.log = function(logMessage){
        // Do something with the log message
        _log.apply(console,arguments);
    };
  
    console.warning = function(warnMessage){
       // do something with the warn message
       _warning.apply(console,arguments);
    };
    
  })();