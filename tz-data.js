'use strict';
const mysql = require('mysql');
const util = require('util'); 

class TzData {
    constructor() {}
    async getConnection(settings) {
        var connection = mysql.createConnection({
            host     : settings.host,
            user     : settings.user,
            password : settings.password,
            database : settings.database,
            dateStrings:true
        });
        connection.queryAsync = util.promisify(connection.query).bind(connection);
        connection.endAsync = util.promisify(connection.end).bind(connection); 
        return new Promise(resolve => { resolve(connection); });
    }
}

module.exports = new TzData();