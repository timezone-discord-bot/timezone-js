'use strict';
const jslinq = require('jslinq');
const dateFormat = require('dateformat');
const tzData = require('./tz-data'); 

class TzGuildSettings {
    constructor(settings) {
        this._settings = settings; 
    }

    //#region isGuildReactionOn
    async isGuildReactionOn(guildId) {
        var guildSettings = await this.getGuildSettings(guildId);
        return new Promise(resolve => resolve(guildSettings.reaction_on == 1 ? true : false)); 
    }
    //#endregion
    async toggleGuildReaction(guildId, userId) { 
        var saveResult;
        var dbConn = await tzData.getConnection(this._settings.mysql);
        try {
            var queryResult = await this.getGuildSettings(guildId, dbConn);
            var guildSettings = {
                guild_id: guildId
            };
            if (queryResult) {
                guildSettings.id = queryResult.id;
                guildSettings.creator_id = queryResult.creator_id;
                guildSettings.create_date = queryResult.create_date;
                guildSettings.updater_id = queryResult.updater_id;
                guildSettings.update_date = queryResult.update_date;
                guildSettings.reaction_on = queryResult.reaction_on == 1 ? 0 : 1;
            }
            else {
                guildSettings.reaction_on = 1;
            }
            saveResult = await this.saveGuildSettings(guildSettings, userId, dbConn);
        }
        catch(err) {
            console.error(err);
            saveResult = err;
        } 
        finally {
            await dbConn.endAsync(); 
        }
        return new Promise(resolve => resolve(guildSettings.reaction_on == 1 ? true : false)); 
    }

    async getGuildSettings(guildId, dbConn) {
        var madeCon = false;
        if (!dbConn) {
            dbConn = await tzData.getConnection(this._settings.mysql);
            madeCon = true;
        }
        var queryResult = new jslinq(await dbConn.queryAsync(`SELECT id, CAST(guild_id AS CHAR(20)) AS guild_id, reaction_on, gmt_offset, dst, 
                                                                     create_date, CAST(creator_id AS CHAR(20)) AS creator_id, update_date, 
                                                                     CAST(updater_id  AS CHAR(20)) AS updater_id 
                                                              FROM guild_settings 
                                                              WHERE guild_id=${guildId}`)).firstOrDefault();
        if (madeCon) {
            await dbConn.endAsync(); 
        }
        return new Promise(resolve => resolve(queryResult));  
    }

    async saveGuildSettings(guildSettings, userId, dbConn) {
        if (guildSettings && guildSettings.guild_id && userId && 
            (
                typeof guildSettings.reaction_on !== 'undefined' || 
                typeof guildSettings.gmt_offset !== 'undefined' || 
                typeof guildSettings.dst !== 'undefined' 
            )
        ) {
            var madeCon = false;
            if (!dbConn) {
                dbConn = await tzData.getConnection(this._settings.mysql);
                madeCon = true;
            }
            var result;
            try {
                var exists = false;
                if (guildSettings.id && 
                    guildSettings.creator_id && 
                    guildSettings.create_date && 
                    guildSettings.updater_id && 
                    guildSettings.update_date) {
                    exists = true;
                }
                else {
                    if (await this.getGuildSettings(guildSettings.guild_id, dbConn)) {                      
                        exists = true;
                    }
                }
                if (exists) {
                    var setText = '';
                    if (typeof guildSettings.reaction_on !== 'undefined') {
                        if (setText != '') setText += ', ';
                        setText += `reaction_on=${guildSettings.reaction_on == null ? '0' : guildSettings.reaction_on}`;
                    }
                    if (typeof guildSettings.gmt_offset !== 'undefined') { 
                        if (setText != '') setText += ', ';
                        setText += `gmt_offset=${guildSettings.gmt_offset == null ? 'null' : guildSettings.gmt_offset}`;
                    }
                    if (typeof guildSettings.dst !== 'undefined') {
                        if (setText != '') setText += ', ';
                        setText += `dst=${guildSettings.dst == null ? 'null' : guildSettings.dst}`;
                    }
                    setText += `, updater_id=${userId}, update_date='${dateFormat(new Date(),'yyyy-mm-dd hh:MM:ss')}'`;
                    var updateStmt = `UPDATE guild_settings SET ${setText} WHERE guild_id=${guildSettings.guild_id};`;
                    await dbConn.queryAsync(updateStmt)
                        .then(function(res) { result = res; })
                        .catch(function(err) {
                            console.error(err);
                            result = err;
                        });
                }
                else {
                    var columnsText = '';
                    var valuesText = '';
                    if (typeof guildSettings.reaction_on !== 'undefined' && guildSettings.reaction_on != null) {
                        if (columnsText != '') columnsText += ', ';
                        columnsText += `reaction_on`;
                        if (valuesText != '') valuesText += ', ';
                        valuesText += `${guildSettings.reaction_on}`;
                    }
                    if (guildSettings.gmt_offset) {                
                        if (columnsText != '') columnsText += ', ';
                        columnsText += `gmt_offset`;
                        if (valuesText != '') valuesText += ', ';
                        valuesText += `${guildSettings.gmt_offset}`;
                    }
                    if (guildSettings.dst) {
                        if (columnsText != '') columnsText += ', ';
                        columnsText += `dst`;
                        if (valuesText != '') valuesText += ', ';
                        valuesText += `${guildSettings.dst}`;
                    }
                    result = await dbConn.queryAsync(`INSERT INTO guild_settings (guild_id, ${columnsText}, creator_id, updater_id) ` + 
                                                     `VALUES (${guildSettings.guild_id},${valuesText},${userId},${userId});`);                                        
                }
            }
            catch (err) {
                console.error(err);
                result = err;
            }
            finally {
                if (madeCon) {
                    await dbConn.endAsync(); 
                }
                return new Promise(resolve => resolve(result));
            }
        }
        return new Promise(reject => reject('Cannot save Guild Settings, invalid input!'));  
    }
}

module.exports = TzGuildSettings; 