'use strict';
class TzObjectUtil {
    static copyObjectGraph(source, destination, usePrivateFields) {
        if (!usePrivateFields)
            usePrivateFields = false; 
        if (source && (typeof source == 'object')) {
            var setRef; 
            if (!destination || typeof destination != 'object') {
                // Calling method gave us an undefined or null for 'destination' 
                // parameter so we cannot proceed, primitive types (null & undefined 
                // are primitive types) cannot be passed back to caller by reference. 
                throw new Error(`TzObjectUtil.copyObjectGraph method cannot proceed because 'destination' parameter is not a reference type! \r\n` + 
                                `Type was: ${typeof destination} \r\n` + 
                                `Value was: ${destination}`); 
            }
            else {
                // 'destination' parameter is an object.. 
                // we're good to just set it to the 'set reference' object 
                setRef = destination; 
            }
            // iterate the properties of the source object 
            for (property in source) {
                // get source object's value for current property key, set aside to var 
                var propValInitial = source[property];
                // define var for the final property value- will be used further 
                // below, and needs to be defined here outside of the if/else 
                // block below it so it is still in scope for the final Reflect.set 
                var propValFinal;
                // if callered asked for setting private fields then for the 
                // 'destination' property key we need to prefix it with '_' 
                var destProperty;
                if (usePrivateFields) {
                    // using private fields for Reflect.set so we need to prefix 
                    // the property key with underscore '_' & lowercase the first char 
                    destProperty = `_${property.substring(0,1).toLowerCase()}`;
                    if (property.size > 1) {
                        // property key is not 1 char long so substring the rest of it 
                        destProperty = `${destProperty}${property.substring(1,(property.size-1))}`;
                    }
                }
                else {
                    destProperty = property; 
                }
                if (typeof propValue == 'object') {
                    // We encountered a nested reference type (object) as one of 
                    // source's properties.  We will have to use recursion to 
                    // ensure we copy the object's entire graph. 
                    // But first- does the 'destination' object have a property defined for 
                    // the current 'property' key and has an object reference for its value? 
                    if (!Reflect.has(destination, property)) {
                        // 'destination' object does NOT have a 'property' key defined 
                        // so let's define one now 
                        Reflect.defineProperty(destination, property); 
                    }
                    // get 'destination' object's value for the 'property' key 
                    var destValInitial = destination[destProperty]; 
                    // just having property is not enough though! we need to make sure 
                    // it is an object reference (i.e. not undefined, null, or any 
                    // primitive type) 
                    if (typeof destValInitial != 'object') {
                        // regardless of reason for failing the above object ref check 
                        // the solution is the same: set its value to an empty object ref 
                        destValInitial = { };
                    }
                    // Call this same method recursively, supplying for the next method call's 
                    // 'source' parameter the initial property value we set above, and for 
                    // the 'destination' parameter we will do the same for destination's counterpart 
                    propValFinal = TzObjectUtil.copyObjectGraph(propValInitial, destValInitial); 
                }
                else {
                    // Property is primitive so a simple value copy will suffice. 
                    propValFinal = propValInitial; 
                }
                // Now we should have what we need to decide whether we need to set 
                // the value at all.  Perform simple object ref equals check '==='. 
                // This will verify whether the finalized source property value 
                // and the destination's property value... 
                //  - For Primitive Types (Boolean,Null,Undefined,Number,String,Symbol): 
                //      ... are the same type and contain the same primitive value. 
                //  - For Reference Types (Object): 
                //      ... reference the same object in memory (object hash matches). 
                if (propValFinal !== destination[destProperty]) {
                    // our finalized source property value and the destination property value do not match 
                    // we should set the finalized source property value to the destination object reference 
                    // it is important we are mutating the destination object in the caller's scope so 
                    // we are using Reflect.set 
                    Reflect.set(destination, destProperty); 
                }
                // no else needed here because if we matched on finalized source & destination property 
                // value then there is no need to change any value(s) 
            }
        }
    }
}
module.exports = TzObjectUtil;