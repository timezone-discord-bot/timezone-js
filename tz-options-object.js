'use strict';
const TzObjectUtil = require('./tz-object-util');

class TzOptionsObject {
    constructor(options, usePrivateFields) {
        TzObjectUtil.copyObjectGraph(options, this, usePrivateFields);
    }
}
module.exports = TzOptionsObject; 