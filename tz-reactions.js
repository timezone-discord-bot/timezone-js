'use strict';
const util = require('util');
const jslinq = require('jslinq');
const TzGuildSettings = require('./tz-guild-settings'); 
const TzUserSettings = require('./tz-user-settings'); 
const tzData = require('./tz-data'); 

class TzReactions { 
    constructor(settings, discordClient) { 
        this._regex = /(([0-1]?[0-9])(:[0-5][0-9])?\s*(AM|PM)|(2[0-3]|[0-1]?[0-9]):([0-5][0-9])?)\s*([+-]\d{2,4}|[a-z]{3,4})?/gi;
        this._settings = settings;
        this._discordClient = discordClient;
        this._guildSettings = new TzGuildSettings(this._settings); 
        this._userSettings = new TzUserSettings(this._settings); 
    }
    get options() {
        return this._settings; 
    }
    get discordClient() {
        return this._discordClient; 
    }
    get currentUser() {
        if ((this._discordClient) && (this._discordClient.user)) {
            return this._discordClient.user;
        }
        return null;
    }

    async parseForReaction(msg) {
        if (msg & msg.guild && msg.guild.id && msg.content) { 
            // ensure guild has reactions turned on first 
            if (await this._guildSettings.isGuildReactionOn(msg.guild.id)) {
                // ok, reactions are a go, parse for time text 
                var matches = msg.content.match(this._regex);
                if (matches && matches.length > 0) {
                    await setTimeout(() => { msg.react('🕰'); }, 2000);;
                }
            }
        }
    }

    async reactToTimeText(user, msgReact) {

        var regex = new RegExp(this._regex);
        var match;
        var matches = [];
        do {
            match = regex.exec(msgReact.message.content);
            if (match) {
                matches.push(match);
            }
        } while (match)

        //let matches = [ msgReact.message.content.matchAll(this._regex) ];
        if (matches.length > 0) {
            var codeBlockTag = '```';
            var msgText = `Found ${matches.length} time(s) in message...\r\n` + 
                          `${codeBlockTag}Channel: ${msgReact.message.channel.name} | Author: ${msgReact.message.author.username} | Sent: ${msgReact.message.createdAt}\r\n\r\n` + 
                          `${msgReact.message.content}${codeBlockTag}\r\n` + 
                          `Time(s) found...\r\n`;
            for(var i = 0; i < matches.length; i++) {
                msgText += `\t- ${util.format(matches[i])}\r\n`; 
            }
            var guildSettings, userSettings;
            var dbConn = await tzData.getConnection(this._settings.mysql);
            try {
                guildSettings = await this._guildSettings.getGuildSettings(msgReact.message.guild.id, dbConn);
                userSettings = await this._userSettings.getUserSettings(user.id, dbConn);
            }
            finally {
                if (dbConn) {
                    await dbConn.endAsync(); 
                }
            }
            
            if (guildSettings) {
                console.log(`server has gmt offset: ${guildSettings.gmt_offset}`);
            }
            else {
                msgText += `Your Discord server does not have its time set, see *help*.`;
                await user.send(msgText);
            }
            
            if (userSettings) {
                console.log(`user has gmt offset: ${userSettings.gmt_offset}`);
            }
            else {
                msgText += `You do not have your local time set, see *help*.`;
                await user.send(msgText);
            }
        }
    }
}

module.exports = TzReactions; 