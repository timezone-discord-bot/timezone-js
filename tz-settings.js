'use strict';
const TzOptionsObject = require('./tz-options-object');

class TzSettingsMySql extends TzOptionsObject {    
    constructor(options) {
        super(options, true); 
        this._host = 'localhost';
        this._database = 'tzdb';
        this._user = 'demouser';
        this._password = process.env.TZ_BOT_DB_PWD;
    }
    get host() {
        return this._host;
    }
    get database() {
        return this._database;
    }
    get user() {
        return this._user;
    }
    get password() {
        return this._password;
    }
}

class TzSettingsRedis extends TzOptionsObject {
    constructor(options) {
        super(options, true);
        this._host = 'localhost';
        this._port = 6379;
        this._db = 11;
    }
    get host() {
        return this._host;
    }
    get port() {
        return this._port;
    }
    get db() {
        return this._db;
    }
}

class TzSettingsDiscord extends TzOptionsObject {
    constructor(options) {
        super(options, true);
        this._token = process.env.TZ_BOT_TOKEN; 
        /*
        if (options) {
            if (options.token) 
                this._token = options.token;
        }
        */
    }
    get token() {
        return this._token; 
    }
}

class TzSettings extends TzOptionsObject {
    constructor(options) {
        if (!options)
            options = {};
        super(options, true);
        this._mysql = new TzSettingsMySql(options.mysql);
        this._redis = new TzSettingsRedis(options.redis);
        this._discord = new TzSettingsDiscord(options.discord);
    }
    get mysql() {
        return this._mysql;
    }
    get redis() {
        return this._redis;
    }
    get discord() {
        return this._discord;
    }
}

exports.TzSettingsMySql = TzSettingsMySql; 
exports.TzSettingsRedis = TzSettingsRedis; 
exports.TzSettingsDiscord = TzSettingsDiscord; 
exports.TzSettings = TzSettings; 