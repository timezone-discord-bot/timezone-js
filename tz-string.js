'use strict';
class TzString {
    isNumeric(input) {
        return ((typeof input == "number" && !isNaN(input)) || 
            (Number(input) != NaN));
    }
    isNumberOnlyText(input) {
        if (typeof input == "string") {
            var foundAlpha = false;
            var numbers = '0123456789';
            for(var i = 0; i < input.length; i++) {
                if (!numbers.includes(input[i])) {
                    foundAlpha = true;
                    break;
                }
            }
            return !foundAlpha; 
        }
        return false;
    }
    hasText(input) {
        return (input && input != null && typeof input == 'String' && input.trim() != '');
    }
}
module.exports = new TzString(); 