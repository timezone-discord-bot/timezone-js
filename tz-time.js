'use strict';
const dateFormat = require('dateformat');

class TzTime {
    constructor() { 
        this._regex = /(([0-1]?[0-9])(:[0-5][0-9])?\s*(AM|PM)|(2[0-3]|[0-1]?[0-9]):([0-5][0-9])?)\s*([+-]\d{2,4}|[a-z]{3,4})?/i;
    }

    getTimeDisplay(date) {
        var h = date.getUTCHours();
        var m = date.getUTCMinutes();
        var hTxt = '';
        var ttTxt = '';
        if (h > 12) {
            hTxt = (h - 12).toString();
            ttTxt = 'PM';
        }
        else if (h == 0) {
            hTxt = '12';
            ttTxt = 'AM';
        }
        else if (h == 12) {
            hTxt = '12';
            ttTxt = 'PM';
        }
        else {
            hTxt = h.toString();
            ttTxt = 'AM';
        }
        var mTxt = m.toString();
        if (mTxt.length == 1) {
            mTxt = `0${mTxt}`;
        }
        return `${hTxt}:${mTxt} ${ttTxt}`;
    }
    getCurrentTimeOffset(gmt_offset) {
        var d = new Date();
        d.setUTCSeconds(d.getUTCSeconds() + gmt_offset);
        return this.getTimeDisplay(d);
    }
    getGMTOffset(hours, tt) {
        if (tt && tt.toLowerCase() == 'am' && 
            hours == 12) { 
            hours = 0;
        }
        if (tt && tt.toLowerCase() == 'pm') {
            hours += 12;
            if (hours >= 24) {
                hours -= 24;
            }
        }
        var utc = new Date();
        var utcHours = utc.getUTCHours(); 
        var utcMinutes = utc.getUTCMinutes(); 
        if (utcHours != 0 || utcMinutes != 0) {
            if (utcHours >= 12) {
                var hoursMin = (utcHours + 12) - 24 + 1;
                if (hours < hoursMin) {
                    hours += 24;
                }
            }
            else {
                var hoursMax = (utcHours + 12);
                if (hours > hoursMax) {
                    hours -= 24;
                }
            }
        }
        var diffHours = hours - utcHours;
        return (diffHours * 60 * 60);
    }
    getTimeComponents(time) {
        if (time) {
            var result = time.match(this._regex);
            if (result && result.length > 0) {
                return {
                    hours: parseInt(result[2]),
                    minutes: result[3] ? parseInt(result[3].substring(1)) : 0,
                    tt: result[4]
                }
            }
            else {
                return null;
            }
        }
    }
}

module.exports = new TzTime();