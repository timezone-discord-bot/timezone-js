'use strict';
const jslinq = require('jslinq');
const dateFormat = require('dateformat');
const tzData = require('./tz-data'); 

class TzUserSettings {
    constructor(settings) {
        this._settings = settings; 
    }

    async getUserSettings(userId, dbConn) {
        var madeCon = false;
        if (!dbConn) {
            dbConn = await tzData.getConnection(this._settings.mysql);
            madeCon = true;
        }
        var queryResult = new jslinq(await dbConn.queryAsync(`SELECT id, CAST(user_id AS CHAR(20)) AS user_id, gmt_offset, dst, 
                                                                     create_date, CAST(creator_id AS CHAR(20)) AS creator_id, update_date, 
                                                                     CAST(updater_id  AS CHAR(20)) AS updater_id 
                                                              FROM user_settings 
                                                              WHERE user_id=${userId}`)).firstOrDefault();
        if (madeCon) {
            await dbConn.endAsync(); 
        }
        return new Promise(resolve => resolve(queryResult));  
    }

    async saveUserSettings(userSettings, userId, dbConn) {
        if (userSettings && userSettings.user_id && userId && 
            (
                typeof userSettings.gmt_offset !== 'undefined' || 
                typeof userSettings.dst !== 'undefined'
            )
        ) {
            var madeCon = false;
            if (!dbConn) {
                dbConn = await tzData.getConnection(this._settings.mysql);
                madeCon = true;
            }
            var exists = false;
            if (userSettings.id && 
                userSettings.creator_id && 
                userSettings.create_date && 
                userSettings.updater_id && 
                userSettings.update_date) {
                exists = true;
            }
            else {
                if (await this.getUserSettings(userSettings.user_id, dbConn)) {
                    exists = true;
                }
            }
            if (exists) {
                var setText = '';
                if (typeof userSettings.gmt_offset !== 'undefined') {                
                    if (setText != '') setText += ', ';
                    setText += `gmt_offset=${userSettings.gmt_offset}`;
                }
                if (typeof userSettings.dst !== 'undefined') {
                    if (setText != '') setText += ', ';
                    setText += `dst=${userSettings.dst}`;
                }
                setText += `, updater_id=${userId}, update_date='${dateFormat(new Date(),'yyyy-mm-dd hh:MM:ss')}'`;
                var updateResult = await dbConn.queryAsync(`UPDATE user_settings 
                                                                SET ${setText} 
                                                            WHERE user_id=${userSettings.user_id};`);
                if (madeCon) {
                    await dbConn.endAsync(); 
                }
                return new Promise(resolve => resolve(updateResult));  
            }
            else {
                var columnsText = '';
                var valuesText = '';
                if (userSettings.gmt_offset) {                
                    if (columnsText != '') columnsText += ', ';
                    columnsText += `gmt_offset`;
                    if (valuesText != '') valuesText += ', ';
                    valuesText += `${userSettings.gmt_offset}`;
                }
                if (userSettings.dst) {
                    if (columnsText != '') columnsText += ', ';
                    columnsText += `dst`;
                    if (valuesText != '') valuesText += ', ';
                    valuesText += `${userSettings.dst}`;
                }
                var insertResult = await dbConn.queryAsync(`INSERT INTO user_settings (user_id, ${columnsText}, creator_id, updater_id) ` + 
                                                           `VALUES (${userSettings.user_id},${valuesText},${userId},${userId});`);
                if (madeCon) {
                    await dbConn.endAsync(); 
                }
                return new Promise(resolve => resolve(insertResult));                                            
            }
        }
        return new Promise(reject => reject('Cannot save User Settings, invalid input!'));  
    }
}

module.exports = TzUserSettings; 